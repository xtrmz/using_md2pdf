# Titre

## Titre 2


* A
* B
* C


## Un peu de code


```yaml 

---
stages:
  - build
  - deploy

#slides-offline:
#  tags:
#    - docker
#  stage: build
#  image: conoria/alpine-pandoc
#  variables:
#    http_proxy: "http://devwatt-proxy.si.fr.intraorange:8080"
#    https_proxy: "http://devwatt-proxy.si.fr.intraorange:8080"
#  script:
#    - wget -O - \
#      https://github.com/hakimel/reveal.js/archive/3.7.0.tar.gz \
#      | tar xfz -
#    - pandoc --include-in-header=css/adjust.css --metadata-file=index.yaml --standalone -t revealjs --variable=transition:slides --variable=theme:black --variable=revealjs-url:reveal.js-3.7.0 -f markdown+smart+emoji index.md -o index-offline.html
#  artifacts:
#    paths:
#      - index-offline.html
#      - reveal.js-3.7.0/
#    expire_in: 1 week

slides:
  stage: build
  image: conoria/alpine-pandoc
  script:
    - pandoc --include-in-header=css/adjust.css --metadata-file=index.yaml --standalone -t revealjs --variable=transition:slides --variable=theme:white --variable=revealjs-url:https://revealjs.com/ -f markdown+smart+emoji index.md -o index.html
  artifacts:
    paths:
      - index.html
    expire_in: 1 week

pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
#  only:
#    - master



```




```shell

for f in $files
do
   convert -geometry 800x600 f $(basename $f).png
done


```




